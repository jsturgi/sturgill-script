﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




    public class Person
    {
        public int Age; // determines age

        public string Sex; // details sex

        public float Money; // details money

        public float lifeScore; // details life score

        public static int population = 0;

        public Person() // child class constructor
        {
            Age = 0;

            Money = 0;

            lifeScore = 0;

            population++;

            Sex = Random.Range(1, 2) == 1 ? "male" : "female";

            Debug.Log("a child was born :D");
        }



    }
