﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class middleClass : Person
{
    public string job;

    public bool spouse;

    public string house;

    public List<string> jobs = new List<string>() { "Gas Station Attendant", "Grocery Worker", "Cop", "Chipotle GM", "Teacher", "HR", "Accountant", "Game Designer", "Lawyer", "Doctor" };
    
    public List<string> houses = new List<string>() { "Park Bench", "Tent", "Brothel", "Apartment", "Condo", "One Story", "Ranch", "Upscale House", "Mansion", "Estate" };


    public middleClass()
    {
        job = jobs[Random.Range(0, 10)];

        house = houses[Random.Range(0, 10)];

        spouse = Random.Range(1, 2) == 1 ? true : false;

        population++;


    }
}
